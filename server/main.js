import { Meteor } from 'meteor/meteor';
import '../imports/api/users.js';
import '../imports/api/images.js';

Accounts.onCreateUser((options, user) => {
    /*
     *  user.profile is editable by default by the user
     */
    user.profile = {
        name: user.emails[0].address.match(/(.+)@.+/)[1],
        bio: ""
    };
    return user;
});

Meteor.startup(() => {
    // code to run on server at startup
});
