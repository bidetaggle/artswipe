export const uiCallback = (message) => {
    if ((Meteor.isCordova && Platform.isAndroid())) {
        window.plugins.toast.showShortCenter(message);
    }
    else {
        alert(message);
    }
};
