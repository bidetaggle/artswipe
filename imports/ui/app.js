import { Accounts } from 'meteor/accounts-base';

import './login.js';
import './main.js';
import './profile.js';
import './image.js';

import './app.html';

Template.app.helpers({
    activeNavLink(navLink) {
        const active =
            navLink === FlowRouter.getParam('username')
            ||
            navLink == 'users-list'
            && ActiveRoute.name('users-list');

        return active && "activeNavLink";
    },
    is_webBrowser() {
        return Platform.isWebBrowser();
    },
    is_connected() {
        return Meteor.status().connected;
    }
});

Template.app.events({
    'click': function(evt) {
        if (evt.target.id === "addLink") {
            var modale = document.getElementById('showLink')
            modale.style.display = (modale.dataset.toggled ^= 1) ? "flex" : "none";
        }
    }
});
