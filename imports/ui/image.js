import { Template } from 'meteor/templating';
import { Images } from '../api/images.js';

import './image.html';

Template.image.helpers({
    image(){
        return Images.findOne({_id: FlowRouter.getParam('_id')});
    }
});

Template.image.events({
    'submit'(event){
        event.preventDefault();
        console.log(this.userId);
        const input = event.target.getElementsByTagName('input')[0];
        Meteor.call('image.update', FlowRouter.getParam('_id'), input.name, input.value);
    }
});
