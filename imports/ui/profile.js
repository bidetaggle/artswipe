import { Template } from 'meteor/templating';
import { Random } from 'meteor/random';
import { Meteor } from 'meteor/meteor';
import { Images } from '../api/images.js';
import { ReactiveVar } from 'meteor/reactive-var';

import './profile.html';

Template.profile.onCreated(function () {
    this.currentUpload = new ReactiveVar(false);
});

Template.profile.helpers({
    currentUpload() {
        return Template.instance().currentUpload.get();
    },
    images(){
        let imgs = Images.find(
            {userId: Meteor.userId()},
            {sort: {createdAt: -1}}
        ).each();
        //return imgs.count() > 0 ? imgs : false;

        return imgs;
    }
});

Template.profile.events({
    'submit form'(event){
        event.preventDefault();

        const username = event.target.username.value;
        const userbio = event.target.userbio.value;

        Meteor.call('user.update', username, userbio);
    },
    'change #fileInput'(e, template) {
        if (e.currentTarget.files && e.currentTarget.files[0]) {
            // We upload only one file, in case
            // multiple files were selected
            const upload = Images.insert({
                file: e.currentTarget.files[0],
                streams: 'dynamic',
                chunkSize: 'dynamic'
            }, false);

            upload.on('start', function () {
                template.currentUpload.set(this);
            });

            upload.on('end', function (error, fileObj) {
                if (error) {
                    alert('Error during upload: ' + error);
                } else {
                    alert('File "' + fileObj.name + '" successfully uploaded');
                    console.log(fileObj);
                }
                template.currentUpload.set(false);
            });

            upload.start();
        }
    },
    'click .remove'(){
        console.log(this);
        Meteor.call("image.remove", this._id, this.userId);
    }
});
