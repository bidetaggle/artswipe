import { Template } from 'meteor/templating';

import './tinder.html';

let moving = false;

if (Meteor.isClient) {
Template.tinder.helpers({
    hammerInitOptions: function(){
        return function(hammer, templateInstance) {
            let multiDirectionalPan = new Hammer.Pan({
                direction: Hammer.DIRECTION_ALL
            });
            hammer.add(multiDirectionalPan);
            return hammer;
        }
    },
    templateGestures: {
        'pan .tinder .tinder--cards .tinder--card'(event, templateInstance){
            console.log('start')
            event.target.classList.add('moving');

            if (event.deltaX === 0) return;
            if (event.center.x === 0 && event.center.y === 0) return;

            // tinderContainer.classList.toggle('tinder_love', event.deltaX > 0);
            // tinderContainer.classList.toggle('tinder_nope', event.deltaX < 0);

            var xMulti = event.deltaX * 0.03;
            var yMulti = event.deltaY / 80;
            var rotate = xMulti * yMulti;

            event.target.style.transform = 'translate(' + event.deltaX + 'px, ' + event.deltaY + 'px) rotate(' + rotate + 'deg)';
        },
        'panend .tinder .tinder--cards .tinder--card'(event, templateInstance){
            console.log('end');
            event.target.classList.remove('moving');

            var moveOutWidth = document.body.clientWidth;
            var keep = Math.abs(event.deltaX) < 80 || Math.abs(event.velocityX) < 0.5;

            event.target.classList.toggle('removed', !keep);

            if (keep) {
              event.target.style.transform = '';
            } else {
              var endX = Math.max(Math.abs(event.velocityX) * moveOutWidth, moveOutWidth);
              var toX = event.deltaX > 0 ? endX : -endX;
              var endY = Math.abs(event.velocityY) * moveOutWidth;
              var toY = event.deltaY > 0 ? endY : -endY;
              var xMulti = event.deltaX * 0.03;
              var yMulti = event.deltaY / 80;
              var rotate = xMulti * yMulti;

              event.target.style.transform = 'translate(' + toX + 'px, ' + (toY + event.deltaY) + 'px) rotate(' + rotate + 'deg)';
              //initCards();
            }
        }
    }
});
}
Template.tinder.events({

});
