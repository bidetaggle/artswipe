import { Meteor } from 'meteor/meteor';
import { FilesCollection } from 'meteor/ostrio:files';

export const Images = new FilesCollection({
    collectionName: 'Images',
    allowClientCode: true, // Allow remove files from Client
    storagePath: '../../../../../../data_artswipe',
    onBeforeUpload(file) {
        // Allow upload files under 10MB, and only in png/jpg/jpeg formats
        if (file.size <= 10485760 && /png|jpg|jpeg/i.test(file.extension)) {
            return true;
        }
        return 'Please upload image, with size equal or less than 10MB';
    },
    onBeforeRemove(cursor) {
        const records = cursor.fetch();
        if (records && records.length) {
            if (this.userId) {
                const user = this.user();
                // Assuming user.profile.docs is array
                // with file's records _id(s)

                for (let i = 0, len = records.length; i < len; i++) {
                    const file = records[i];
                    if (!~user.profile.docs.indexOf(file._id)) {
                        // Return false if at least one document
                        // is not owned by current user
                        return false;
                    }
                }
            }
        }
        return true;
    }
});

if (Meteor.isClient) {
  Meteor.subscribe('files.images.all');
}

if (Meteor.isServer) {
  Meteor.publish('files.images.all', function () {
    return Images.find().cursor;
  });
}

Meteor.methods({
    'image.remove'(_id, userId){
        Images.remove({_id: _id});
    },
    'image.update'(_id, data, value){
        check(data, String);
        check(value, String);

        if(!Meteor.user() || Meteor.userId() != Images.findOne({_id: _id}).userId)
            throw new Meteor.Error('not-authorized');

        Images.update(
            {_id: _id},
            { $set:
                data
            }
        );

    }
});
