import { Meteor } from 'meteor/meteor';
import { Mongo } from 'meteor/mongo';

if (Meteor.isServer) {
    Meteor.publish('usersList', () => {
        return Meteor.users.find({},{fields: {username: 1}});
    });
}

Meteor.methods({
    'user.update'(username, userbio){
        if(!Meteor.user()) throw new Meteor.Error('not-authorized');

        check(username, String);
        check(userbio, String);

        let profile = {
            name: username,
            bio: userbio
        }

        Meteor.users.update(Meteor.userId(), {$set: {profile: profile}});
    }
});
