# artswipe #

The tinder of art!

# Development #

## install & run ##

- `$ npm i`
- `$ meteor`       run
- `$ meteor mongo` database access

## run on android ##

- `$ meteor add-platform android` (To do only once)
- `$ meteor run android-device`

## tests ##

In order to enter in test mode, you'll need to stop the regular app from running, or specify an alternate port with `--port XYZ`

`$ meteor test --driver-package meteortesting:mocha`
