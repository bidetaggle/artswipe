import '../imports/ui/app.js';

//Accounts.onLogin(() => {
    //get fired when refreshing /profile for no reason
    //FlowRouter.go('/');
//});

Accounts.onLogout(() => {
    setTimeout(() => FlowRouter.go('login'), 0);
});

AccountsTemplates.configure({
    onSubmitHook: ( error, state ) => {
        if ( !error && state === 'signIn' ) {
            // login successful, route to index
            FlowRouter.go('/');
        }
    }
});

FlowRouter.route('/login', {
    name: 'login',
    action(params, queryParams) {
        if(!Meteor.userId())
        BlazeLayout.render('app', {main: 'login'});
        else
        FlowRouter.go('/');
    }
});

const loggedInRoutes = FlowRouter.group({
    prefix: '',
    name: '',
    triggersEnter: [function(context, redirect) {
        if(!Meteor.userId())
            redirect('/login');
    }]
});

loggedInRoutes.route('/', {
    name: 'index',
    action(params, queryParams) {
            BlazeLayout.render('app', {main: 'main'});
    }
});

loggedInRoutes.route('/profile', {
    name: 'profile',
    action(params, queryParams) {
        BlazeLayout.render('app', {main: 'profile'});
    }
});

loggedInRoutes.route('/image/:_id', {
    name: 'image',
    action(params, queryParams) {
        BlazeLayout.render('app', {main: 'image'});
    }
});
