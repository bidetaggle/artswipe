App.info({
    name: 'artswipe',
    description: "artswipe",
    author: "bidetaggle",
    email: "bidetaggle@protonmail.com",
    website: "https://bidetaggle.com",
    id: 'com.artswipe.release',
    version: "0.0.2"
});

App.icons({
    'android_mdpi': 'icons/icon-48x48.png',
    'android_hdpi': 'icons/icon-72x72.png',
    'android_xhdpi': 'icons/icon-96x96.png',
    'android_xxhdpi': 'icons/icon-144x144.png',
    'android_xxxhdpi': 'icons/icon-192x192.png'
});

App.launchScreens({
    'android_mdpi_portrait': 'icons/launchscreen-320x480.png',
    'android_hdpi_portrait': 'icons/launchscreen-480x800.png',
    'android_xhdpi_portrait': 'icons/launchscreen-720x1280.png',
    'android_xxhdpi_portrait': 'icons/launchscreen-960x1600.png',
    'android_xxxhdpi_portrait': 'icons/launchscreen-1280x1920.png'
});

App.setPreference('BackgroundColor', '0xff272931');
App.setPreference('HideKeyboardFormAccessoryBar', false);
App.setPreference("orientation", "portrait");
App.setPreference("FullScreen", true);
//App.setPreference("StatusBarBackgroundColor", "#FFFFFF");

App.accessRule('http://*', {type: 'navigation'});
App.accessRule('https://*', {type: 'navigation'});
